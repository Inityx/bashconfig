use std::{
    collections::VecDeque,
    path::{Path, PathBuf},
    io::{self, Write},
    ffi::{OsStr, OsString},
    os::unix::ffi::{OsStrExt, OsStringExt},
};

const DEFAULT_PATH: &str = "/bin:/usr/bin:/usr/local/bin";

/// Dynamic representation of `$PATH`
#[derive(Debug)]
pub struct PathVar(VecDeque<PathBuf>);

impl PathVar {
    pub fn prepend(&mut self, path: impl Into<PathBuf>) {
        self.0.push_front(path.into());
    }

    pub fn append(&mut self, path: impl Into<PathBuf>) {
        self.0.push_back(path.into());
    }

    pub fn to_os_string(&self) -> OsString {
        use std::slice::SliceConcatExt;

        let vec: Vec<u8> = self.0
            .iter()
            .map(|path| path.as_os_str().as_bytes())
            .collect::<Vec<_>>()
            .join(&b':');

        OsString::from_vec(vec)
    }

    pub fn is_app(&self, filename: impl AsRef<Path>) -> bool {
        self.0.iter().any(|path| path.join(filename.as_ref()).is_file())
    }
}

impl Default for PathVar {
    fn default() -> Self {
        DEFAULT_PATH.into()
    }
}

impl<O: AsRef<OsStr>> From<O> for PathVar {
    fn from(source: O) -> Self {
        Self(source
            .as_ref().as_bytes()
            .split(|&b| b == b':')
            .map(OsStr::from_bytes)
            .map(PathBuf::from)
            .collect()
        )
    }
}

/// Streaming shell configuration writer
pub struct Config<W: Write>(pub W);

impl<W: Write> Config<W> {
    fn write_byte_strings(&mut self, byteses: &[&[u8]]) -> io::Result<()> {
        byteses.iter().map(|&bytes| self.0.write_all(bytes)).collect()
    }

    pub fn source(&mut self, path: impl AsRef<OsStr>) -> io::Result<()> {
        self.write_byte_strings(&[
            b"source \"",
            path.as_ref().as_bytes(),
            b"\"\n",
        ])
    }

    pub fn export(&mut self, key: impl AsRef<OsStr>, val: impl AsRef<OsStr>) -> io::Result<()> {
        self.0.write_all(b"export ")?;
        self.set_var(key, val)
    }

    pub fn set_var(&mut self, key: impl AsRef<OsStr>, val: impl AsRef<OsStr>) -> io::Result<()> {
        self.write_byte_strings(&[
            key.as_ref().as_bytes(),
            b"=\"",
            val.as_ref().as_bytes(),
            b"\"\n"
        ])
    }

    pub fn eval(&mut self, cmd: impl AsRef<OsStr>) -> io::Result<()> {
        self.write_byte_strings(&[
            b"eval \"$(",
            cmd.as_ref().as_bytes(),
            b")\"\n",
        ])
    }

    pub fn alias(&mut self, name: impl AsRef<OsStr>, def: impl AsRef<OsStr>) -> io::Result<()> {
        self.write_byte_strings(&[
            b"alias ",
            name.as_ref().as_bytes(),
            b"=\"",
            def.as_ref().as_bytes(),
            b"\"\n",
        ])
    }

    pub fn set_path(&mut self, path: PathVar) -> io::Result<()> {
        self.set_var("PATH", path.to_os_string())
    }
}
