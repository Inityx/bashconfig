#![deny(clippy::all)]

mod cwd;

use std::{
    env::{args, current_dir},
    path::PathBuf,
    io,
};

use termion::color::{Color, Fg, Reset};
use hostname::get_hostname;

const PWD_RATIO: usize = 3;
const LARGE_GIT_LIMIT: usize = 16;
const SMALL_GIT_LIMIT: usize = 8;
const SMALL_PATH_LIMIT: usize = 12;

struct ColorTriple {
    pub host: &'static dyn Color,
    pub path: &'static dyn Color,
    pub git:  &'static dyn Color,
}

impl ColorTriple {
    pub fn for_host(hostname: impl AsRef<str>) -> Self {
        use termion::color::*;

        match hostname.as_ref() {
            "asgard" => Self { host: &Cyan,  path: &Green,   git: &Yellow },
            "avalon" => Self { host: &Red,   path: &Magenta, git: &Yellow },
            _        => Self { host: &Green, path: &Cyan,    git: &Yellow },
        }
    }
}

struct Ps1 {
    color: ColorTriple,
    dir: PathBuf,
    git: Option<String>,
}

impl Ps1 {
    pub fn new(dir: PathBuf, hostname: impl AsRef<str>) -> Self {
        Self {
            color: ColorTriple::for_host(hostname),
            git: cwd::git_branch(&dir).expect("Git error"),
            dir,
        }
    }

    pub fn large(&self, mut out: impl io::Write, columns: usize) -> io::Result<()> {
        write!(
            out,
            r"(\A){c_host}\u@\h{c_reset}:{c_path}{pwd}{c_reset}",
            c_host  = Fg(self.color.host),
            c_path  = Fg(self.color.path),
            c_reset = Fg(Reset),
            pwd = cwd::dynamic_pwd(&self.dir, columns / PWD_RATIO),
        )?;

        if let Some(git) = &self.git {
            write!(
                out,
                "[{c_git}{branch}{c_reset}]",
                c_git   = Fg(self.color.git),
                c_reset = Fg(Reset),
                branch = cwd::ellipsize(&git, LARGE_GIT_LIMIT),
            )?;
        }

        write!(out, r"\$ ")
    }

    pub fn small(&self, mut out: impl io::Write) -> io::Result<()> {
        write!(
            out,
            r"[{c_host}\u@\h{c_reset}|{c_path}{pwd}{c_reset}",
            c_host  = Fg(self.color.host),
            c_path  = Fg(self.color.path),
            c_reset = Fg(Reset),
            pwd = cwd::ellipsize(
                &self.dir.file_name().expect("Dir is .., somehow").to_string_lossy(),
                SMALL_PATH_LIMIT,
            ),
        )?;

        if let Some(git) = &self.git {
            write!(
                out,
                "|{c_git}{branch}{c_reset}",
                c_git   = Fg(self.color.git),
                c_reset = Fg(Reset),
                branch = cwd::ellipsize(&git, SMALL_GIT_LIMIT),
            )?;
        }

        write!(out, r"]\$ ")
    }
}

fn main() -> io::Result<()> {
    use io::Write;

    let hostname = get_hostname().expect("No hostname found");
    let columns = args().nth(1).expect("Must pass columns")
        .parse::<usize>().expect("Invalid columns number");

    let ps1 = Ps1::new(current_dir()?, hostname);

    let stdout = io::stdout();
    let mut lock = stdout.lock();

    write!(lock, "PS1=\"")?;
    if columns > 100 {
        ps1.large(&mut lock, columns)
    } else {
        ps1.small(&mut lock)
    }?;
    writeln!(lock, "\"")
}
