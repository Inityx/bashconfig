#![feature(slice_concat_ext)]
#![deny(clippy::all)]

mod shell;
use shell::{
    PathVar,
    Config,
};

use std::{
    ffi::OsStr,
    env::var_os,
    os::unix::ffi::OsStrExt,
    process::Command,
    path::PathBuf,
    io,
};


fn main() -> io::Result<()> {
    let stdout = io::stdout();

    let mut config = Config(stdout.lock());
    let homedir  = var_os("HOME").map(PathBuf::from).expect("No $HOME found");
    let mut path = var_os("PATH").map(PathVar::from).unwrap_or_default();

    { // Aliases
        let aliases = homedir.join(".bash_aliases");
        if aliases.is_file() { config.source(aliases)?; }
    }

    // Text editor
    if path.is_app("nvim") {
        config.export("EDITOR", "nvim")?;
        config.alias("vim", "nvim")?;
    } else if path.is_app("vim") {
        config.export("EDITOR", "vim")?;
    } else {
        config.export("EDITOR", "vi")?;
    }

    config.export("VISUAL", "$EDITOR")?;

    // Ruby
    if path.is_app("ruby") {
        if path.is_app("gem") {
            let output = Command::new("ruby").arg("-e").arg("print Gem.user_dir")
                .output().expect("Error getting RubyGem user dir");

            if output.status.success() {
                path.append(OsStr::from_bytes(&output.stdout));
            }
        }

        if path.is_app("rbenv") { config.eval("rbenv init -")?; }
    }

    { // Rust
        let cargo = homedir.join(".cargo");
        if cargo.is_dir() { path.append(cargo); }

        let rustup = homedir.join(".rustup");
        if rustup.is_dir() {
            config.export(
                "RUST_SRC_PATH",
                rustup
                    .join("toolchains")
                    .join(concat!("nightly-", env!("TARGET")))
                    .join("lib/rustlib/src/rust/src")
                    .to_str().unwrap(),
            )?;
        }
    }

    // Local Path
    path.prepend(homedir.join("bin"));
    path.prepend(homedir.join(".local/bin"));

    config.set_path(path)?;
    config.export("PROMPT_COMMAND", homedir.join("bin/prompt_command"))
}
