use std::{
    borrow::Cow,
    env::var_os,
    path::{Path, PathBuf},
};

pub fn ellipsize(source: &str, max_width: usize) -> Cow<str> {
    let len = source.len();
    if len <= max_width { return source.into(); }

    let mut short = String::from("…");
    short.extend(source.chars().skip(len - max_width));
    short.into()
}

fn home_to_tilde(path: &Path) -> Cow<Path> {
    var_os("HOME")
        .and_then(|home| path.strip_prefix(home).ok())
        .map(|stripped| PathBuf::from("~").join(stripped).into())
        .unwrap_or_else(|| path.into())
}

pub fn dynamic_pwd(cwd: impl AsRef<Path>, max_width: usize) -> String {
    let converted = home_to_tilde(cwd.as_ref());
    let mut components = converted.components();

    let min_fit = loop {
        let remaining = components.as_path();
        if remaining.as_os_str().len() <= max_width { break remaining; }
        components.next();
    }.as_os_str();

    if min_fit.is_empty() {
        cwd.as_ref().file_name().expect("Dir is .., somehow")
    } else {
        min_fit
    }.to_string_lossy().into_owned()
}

pub fn git_branch(cwd: impl AsRef<Path>) -> Result<Option<String>, git2::Error> {
    use git2::{Repository, ErrorCode};

    let repo = match Repository::discover(cwd.as_ref()) {
        Ok(repo) => repo,
        Err(error) => return match error.code() {
            ErrorCode::NotFound => Ok(None),
            _ => Err(error),
        },
    };

    let name = match repo.head() {
        Ok(head) => String::from_utf8_lossy(head.shorthand_bytes()).into_owned(),
        Err(error) => match error.code() {
            ErrorCode::UnbornBranch => "<Unborn>".into(),
            _ => return Err(error),
        },
    };

    Ok(Some(name))
}
